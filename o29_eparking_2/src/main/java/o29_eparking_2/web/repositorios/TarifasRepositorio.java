package o29_eparking_2.web.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;	
import org.springframework.stereotype.Repository;
import o29_eparking_2.web.entidades.Tarifas;

@Repository
public interface TarifasRepositorio 
	extends JpaRepository<Tarifas, Integer> {
}
