package o29_eparking_2.web.entidades;

import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name="tipoVehiculos")
public class TipoVehiculos {
	//Atributos de la Clase
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idTipoVehiculo; 
	@Column(name="nombreTipoVehiculo", nullable=false, length=10)
    private String nombreTipoVehiculo; 
	@Column(name="estadoTipoVehiculo", nullable=false, length=10)
    private String estadoTipoVehiculo;
	//Foraneas
	@OneToMany(mappedBy="tipoVehicTarifa")
	private Set<Tarifas> tipoVehicTarifa;
	
	@OneToMany(mappedBy="tipovehiculo")
	private Set<Vehiculos> tipovehiculo;
	
	//Constructores
	public TipoVehiculos() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TipoVehiculos(int idTipoVehiculo, String nombreTipoVehiculo, String estadoTipoVehiculo,
			Set<Tarifas> tipoVehicTarifa, Set<Vehiculos> tipovehiculo) {
		super();
		this.idTipoVehiculo = idTipoVehiculo;
		this.nombreTipoVehiculo = nombreTipoVehiculo;
		this.estadoTipoVehiculo = estadoTipoVehiculo;
		this.tipoVehicTarifa = tipoVehicTarifa;
		this.tipovehiculo = tipovehiculo;
	}

	public int getIdTipoVehiculo() {
		return idTipoVehiculo;
	}

	public void setIdTipoVehiculo(int idTipoVehiculo) {
		this.idTipoVehiculo = idTipoVehiculo;
	}

	public String getNombreTipoVehiculo() {
		return nombreTipoVehiculo;
	}

	public void setNombreTipoVehiculo(String nombreTipoVehiculo) {
		this.nombreTipoVehiculo = nombreTipoVehiculo;
	}

	public String getEstadoTipoVehiculo() {
		return estadoTipoVehiculo;
	}

	public void setEstadoTipoVehiculo(String estadoTipoVehiculo) {
		this.estadoTipoVehiculo = estadoTipoVehiculo;
	}

	public Set<Tarifas> getTipoVehicTarifa() {
		return tipoVehicTarifa;
	}

	public void setTipoVehicTarifa(Set<Tarifas> tipoVehicTarifa) {
		this.tipoVehicTarifa = tipoVehicTarifa;
	}

	public Set<Vehiculos> getTipovehiculo() {
		return tipovehiculo;
	}

	public void setTipovehiculo(Set<Vehiculos> tipovehiculo) {
		this.tipovehiculo = tipovehiculo;
	}

	@Override
	public String toString() {
		return "TipoVehiculos [idTipoVehiculo=" + idTipoVehiculo + ", nombreTipoVehiculo=" + nombreTipoVehiculo
				+ ", estadoTipoVehiculo=" + estadoTipoVehiculo + ", tipoVehicTarifa=" + tipoVehicTarifa
				+ ", tipovehiculo=" + tipovehiculo + "]";
	}

}
