package o29_eparking_2.web.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import o29_eparking_2.web.entidades.TipoVehiculos;
import o29_eparking_2.web.repositorios.TipoVehicRepository;

@Service
public class TipoVehicServicioImp 
	implements TipoVehicServicio {

	@Autowired
	public TipoVehicRepository Repositorio;
	
	@Override
	public List<TipoVehiculos> listarTipoVehic() {
		// TODO Auto-generated method stub
		return Repositorio.findAll();
	}
	

}
