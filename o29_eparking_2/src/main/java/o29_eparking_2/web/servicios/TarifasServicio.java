package o29_eparking_2.web.servicios;

import java.util.List;
import o29_eparking_2.web.entidades.Tarifas;

public interface TarifasServicio {
	
	public List<Tarifas> ListarTarifas(); //Metodo Abstracto
	
	public Tarifas guardarTarifas(Tarifas objTarifas); //Para crear nueva Tarifa
	
	public Tarifas tarifasXid(int id); //Para buscar una tarifa x Id
	
	public Tarifas actualizarTarifas(Tarifas objTarifas); //Actualizar Tarifas
	
	public void eliminarTarifas(int id); 

}
