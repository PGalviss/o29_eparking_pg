package o29_eparking_2.web.repositorios;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import o29_eparking_2.web.entidades.Plazas;

@Repository
public interface PlazasRepository 
	extends JpaRepository <Plazas, Integer > {

}
