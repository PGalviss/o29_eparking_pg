package o29_eparking_2.web.servicios;

import java.util.List;
import o29_eparking_2.web.entidades.TipoVehiculos;

public interface TipoVehicServicio {

	public List<TipoVehiculos> listarTipoVehic();
}
