package o29_eparking_2.web.entidades;

import java.sql.Date;

import javax.persistence.*;

@Entity
@Table(name="factura")
public class Factura {
	//Atributos de la clase
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private double idFactura;
	@Column(name="fecha", nullable=false)
    private Date fecha;  
	@Column(name="valorPagado", nullable=false)
 	private int valorPagado;
	//Foranea
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.LAZY, optional=false)
	@JoinColumn(name="ticketFactura",referencedColumnName="ticket")
 	private Registro ticketFactura;
	
	public Factura() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Factura(double idFactura, Date fecha, int valorPagado, Registro ticketFactura) {
		super();
		this.idFactura = idFactura;
		this.fecha = fecha;
		this.valorPagado = valorPagado;
		this.ticketFactura = ticketFactura;
	}

	public double getIdFactura() {
		return idFactura;
	}

	public void setIdFactura(double idFactura) {
		this.idFactura = idFactura;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public int getValorPagado() {
		return valorPagado;
	}

	public void setValorPagado(int valorPagado) {
		this.valorPagado = valorPagado;
	}

	public Registro getTicketFactura() {
		return ticketFactura;
	}

	public void setTicketFactura(Registro ticketFactura) {
		this.ticketFactura = ticketFactura;
	}

	@Override
	public String toString() {
		return "Factura [idFactura=" + idFactura + ", fecha=" + fecha + ", valorPagado=" + valorPagado
				+ ", ticketFactura=" + ticketFactura + "]";
	} 
	
	
	
}
