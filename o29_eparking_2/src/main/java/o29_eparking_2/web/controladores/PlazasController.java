package o29_eparking_2.web.controladores;

import java.util.List;

import javax.servlet.annotation.WebServlet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import o29_eparking_2.web.entidades.Plazas;
import o29_eparking_2.web.repositorios.PlazasRepository;

@WebServlet("/plazas")
@Controller
public class PlazasController {
	
	@Autowired
	private PlazasRepository plazasRepository;
	
	//Listar las Plazas
	@GetMapping("/plazas/verPlazas")
	public String ListarPlazas(Model modelo) {
		List<Plazas> listaPlazas = plazasRepository.findAll();
		modelo.addAttribute("listaPlazas", listaPlazas);
		return "/plazas/verPlazas";
	}

	//Mostrar Formulario Nueva Plazas
	@GetMapping("/plazas/nuevo")
	public String frmCrearPlazas(Model modelo) {
		modelo.addAttribute("plazas", new Plazas()); //"plazas" que pasa al html
		return "/plazas/crearPlazas"; //archivo htlm que retorna
	}
	
	//Guardar la Plazas nueva
	@PostMapping("/plazas/guardar")
	public String guardarPlazas(Plazas plazas) {
		plazasRepository.save(plazas);
		return "redirect:/plazas/verPlazas";	
	}
	
	//Mostrar Formulario Editar Plazases
	@GetMapping("/plazas/editar/{id}")
	public String frmEditarPlazas(@PathVariable("id") Integer id, Model modelo) {
		Plazas plazas = plazasRepository.findById(id).get();
		modelo.addAttribute("plazas", plazas); //"plazas" que pasa al html
		return "/plazas/editarPlazas"; //archivo htlm que retorna
	}
	
	//Peticion para Actualizar Plazas
	@PostMapping("/plazas/verPlazas/{id}")
	public String editPlazas(@PathVariable int id, 
			@ModelAttribute("plazas") Plazas obj) {
		Plazas plazas = plazasRepository.findById(id).get();
		Plazas plazasExistente = plazas; 
		plazasExistente.setIdPlaza(id); 
		plazasExistente.setEstadoPlaza(obj.getEstadoPlaza());
		plazasExistente.setNombrePlaza(obj.getNombrePlaza());
		plazasRepository.save(plazasExistente);
		return "redirect:/plazas/verPlazas"; 
	}
	
	@GetMapping("/plazas/eliminar/{id}") 
	public String borrarPlazas(@PathVariable("id") Integer id) {
		plazasRepository.deleteById(id);
		return "redirect:/plazas/verPlazas"; 
	}	
}
