package o29_eparking_2.web.servicios;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import o29_eparking_2.web.entidades.Tarifas;
import o29_eparking_2.web.repositorios.TarifasRepositorio;

@Service
public class TarifasServicioImp
	implements TarifasServicio {

	@Autowired
	private TarifasRepositorio repositorio;
	
	@Override
	public List<Tarifas> ListarTarifas() { //Metodo Abstracto
		// TODO Auto-generated method stub
		return repositorio.findAll();
	}

	@Override
	public Tarifas guardarTarifas(Tarifas objTarifas) {
		// TODO Auto-generated method stub
		return repositorio.save(objTarifas);
	}

	@Override
	public Tarifas tarifasXid(int id) {
		// TODO Auto-generated method stub
		return repositorio.findById(id).get();
	}

	@Override
	public Tarifas actualizarTarifas(Tarifas objTarifas) {
		// TODO Auto-generated method stub
		return repositorio.save(objTarifas);
	}
	@Override

	public void eliminarTarifas(int id) {
		repositorio.deleteById(id);
	}

}
