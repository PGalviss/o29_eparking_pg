package o29_eparking_2.web.controladores;

import java.util.List;

import javax.servlet.annotation.WebServlet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import o29_eparking_2.web.entidades.Modalidad;
import o29_eparking_2.web.repositorios.ModalidadRepository;



@WebServlet("/modalidad")
@Controller
public class ModalidadController {
	@Autowired
	private ModalidadRepository modalidadRepository;

	//Listar las Modalidades
	@GetMapping("/modalidad/verModalidad")
	public String ListarModalidad(Model modelo) {
		List<Modalidad> listaModalidad = modalidadRepository.findAll();
		modelo.addAttribute("listaModalidad", listaModalidad);
		return "/modalidad/verModalidad";
	}
	
	//Mostrar Formulario Nueva Modalidad
	@GetMapping("/modalidad/nuevo")
	public String frmCrearModalidad(Model modelo) {
		modelo.addAttribute("modalidad", new Modalidad()); //"modalidad" que pasa al html
		return "/modalidad/crearModalidad"; //archivo htlm que retorna
	}
	
	//Guardar la Modalidad nueva
	@PostMapping("/modalidad/guardar")
	public String guardarModalidad(Modalidad modalidad) {
		modalidadRepository.save(modalidad);
		return "redirect:/modalidad/verModalidad";	
	}
	
	//Mostrar Formulario Editar Modalidades
	@GetMapping("/modalidad/editar/{id}")
	public String frmEditarModalidad(@PathVariable("id") Integer id, Model modelo) {
		Modalidad modalidad = modalidadRepository.findById(id).get();
		modelo.addAttribute("modalidad", modalidad); //"modalidad" que pasa al html
		return "/modalidad/editarModalidad"; //archivo htlm que retorna
	}
	
	//Peticion para Actualizar Modalidad
		@PostMapping("/modalidad/verModalidad/{id}")
		public String editModalidad(@PathVariable int id, 
				@ModelAttribute("modalidad") Modalidad obj) {
			Modalidad modalidad = modalidadRepository.findById(id).get();
			Modalidad modalidadExistente = modalidad; 
			modalidadExistente.setIdModalidad(id); 
			modalidadExistente.setEstadoModalidad(obj.getEstadoModalidad());
			modalidadExistente.setNombreModalidad(obj.getNombreModalidad());
			modalidadRepository.save(modalidadExistente);
		
			return "redirect:/modalidad/verModalidad"; 
		}
	
	@GetMapping("/modalidad/eliminar/{id}") 
	public String borrarModalidad(@PathVariable("id") Integer id) {
		modalidadRepository.deleteById(id);
		return "redirect:/modalidad/verModalidad"; 
	}	
}
