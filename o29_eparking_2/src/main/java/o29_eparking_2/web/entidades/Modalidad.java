	package o29_eparking_2.web.entidades;

import java.util.Set;
import javax.persistence.*;


@Entity
@Table(name="modalidad")
public class Modalidad {
	//Atributos de la Clase
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idModalidad;        
	@Column(name="nombreModalidad", nullable=false, length=10)
    private String nombreModalidad; 
	@Column(name="estadoModalidad", nullable=false, length=10)
    private String estadoModalidad;

	@OneToMany(mappedBy="modalidadVehiculo")
	private Set<Vehiculos> modalidadVehiculo;
	
	@OneToMany(mappedBy="modalidadTarifa")
	private Set<Tarifas> modalidadTarifa;

	//Constructores
	public Modalidad() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Modalidad(int idModalidad, String nombreModalidad, String estadoModalidad, Set<Vehiculos> modalidadVehiculo,
			Set<Tarifas> modalidadTarifa) {
		super();
		this.idModalidad = idModalidad;
		this.nombreModalidad = nombreModalidad;
		this.estadoModalidad = estadoModalidad;
		this.modalidadVehiculo = modalidadVehiculo;
		this.modalidadTarifa = modalidadTarifa;
	}

	public int getIdModalidad() {
		return idModalidad;
	}

	public void setIdModalidad(int idModalidad) {
		this.idModalidad = idModalidad;
	}

	public String getNombreModalidad() {
		return nombreModalidad;
	}

	public void setNombreModalidad(String nombreModalidad) {
		this.nombreModalidad = nombreModalidad;
	}

	public String getEstadoModalidad() {
		return estadoModalidad;
	}

	public void setEstadoModalidad(String estadoModalidad) {
		this.estadoModalidad = estadoModalidad;
	}

	public Set<Vehiculos> getModalidadVehiculo() {
		return modalidadVehiculo;
	}

	public void setModalidadVehiculo(Set<Vehiculos> modalidadVehiculo) {
		this.modalidadVehiculo = modalidadVehiculo;
	}

	public Set<Tarifas> getModalidadTarifa() {
		return modalidadTarifa;
	}

	public void setModalidadTarifa(Set<Tarifas> modalidadTarifa) {
		this.modalidadTarifa = modalidadTarifa;
	}

	@Override
	public String toString() {
		return "Modalidad [idModalidad=" + idModalidad + ", nombreModalidad=" + nombreModalidad + ", estadoModalidad="
				+ estadoModalidad + ", modalidadVehiculo=" + modalidadVehiculo + ", modalidadTarifa=" + modalidadTarifa
				+ "]";
	}

}

