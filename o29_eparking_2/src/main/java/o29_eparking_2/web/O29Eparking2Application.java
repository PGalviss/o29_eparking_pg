package o29_eparking_2.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class O29Eparking2Application {

	public static void main(String[] args) {
		SpringApplication.run(O29Eparking2Application.class, args);
	}

}
