package o29_eparking_2.web.controladores;

import java.util.List;

import javax.servlet.annotation.WebServlet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import o29_eparking_2.web.entidades.TipoVehiculos;
import o29_eparking_2.web.repositorios.TipoVehicRepository;

@WebServlet("/tipoVehiculos")
@Controller
public class TipoVehicController {
	
	@Autowired
	TipoVehicRepository tipoVehicRepository;
	
	//Listar los TipoVehiculo
	@GetMapping("tipoVehiculos/verTipoVehic")
	public String ListarTipoVehiculo(Model modelo) {
		List<TipoVehiculos> listaTipoVehic = tipoVehicRepository.findAll();
		modelo.addAttribute("listaTipoVehic", listaTipoVehic);
		return "/tipoVehiculos/verTipoVehic";
	}

	//Mostrar Formulario Nuevo TipoVehiculo
	@GetMapping("/tipoVehiculos/nuevo")
	public String frmCrearTipoVehiculos(Model modelo) {
		modelo.addAttribute("tipoVehiculos", new TipoVehiculos()); //"tipoVehiculos" que pasa al html
		return "/tipoVehiculos/crearTipoVehic"; //archivo htlm que retorna
	}
	
	//Guardar TipoVehiculo nuevo
	@PostMapping("/tipoVehiculos/guardar")
	public String guardarTipoVehiculos(TipoVehiculos tipoVehiculos) {
		tipoVehicRepository.save(tipoVehiculos);
		return "redirect:/tipoVehiculos/verTipoVehic";	
	}
	
	//Mostrar Formulario Editar TipoVehiculoses
	@GetMapping("/tipoVehiculos/editar/{id}")
	public String frmEditarTipoVehiculos(@PathVariable("id") Integer id, Model modelo) {
		TipoVehiculos tipoVehiculos = tipoVehicRepository.findById(id).get();
		modelo.addAttribute("tipoVehiculos", tipoVehiculos); //"tipoVehiculos" que pasa al html
		return "/tipoVehiculos/editarTipoVehic"; //archivo htlm que retorna
	}
	
	//Peticion para Actualizar TipoVehiculos
	@PostMapping("/tipoVehiculos/verTipoVehic/{id}")
	public String editTipoVehiculos(@PathVariable int id, 
			@ModelAttribute("tipoVehiculos") TipoVehiculos obj) {
		TipoVehiculos tipoVehiculos = tipoVehicRepository.findById(id).get();
		TipoVehiculos tipoVehicExistente = tipoVehiculos; 
		tipoVehicExistente.setIdTipoVehiculo(id); 
		tipoVehicExistente.setEstadoTipoVehiculo(obj.getEstadoTipoVehiculo());
		tipoVehicExistente.setNombreTipoVehiculo(obj.getNombreTipoVehiculo());
		tipoVehicRepository.save(tipoVehicExistente);
		return "redirect:/tipoVehiculos/verTipoVehic"; 
	}
	
	@GetMapping("/tipoVehiculos/eliminar/{id}") 
	public String borrarTipoVehiculos(@PathVariable("id") Integer id) {
		tipoVehicRepository.deleteById(id);
		return "redirect:/tipoVehiculos/verTipoVehic"; 
	}	
}
