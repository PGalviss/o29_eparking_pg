package o29_eparking_2.web.entidades;

import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name="plazas")
public class Plazas {
	//Atributos de la Clase
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idPlaza; 
	@Column(name="nombrePlaza", nullable=false, length=10)	
    private String nombrePlaza;  
	@Column(name="estadoPlaza", nullable=false, length=10)	
    private String estadoPlaza;
	
	@OneToMany(mappedBy="plazaRegistro")
	private Set<Registro> plazaRegistro;
	
	//Constructores
	public Plazas() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Plazas(int idPlaza, String nombrePlaza, String estadoPlaza, Set<Registro> plazaRegistro) {
		super();
		this.idPlaza = idPlaza;
		this.nombrePlaza = nombrePlaza;
		this.estadoPlaza = estadoPlaza;
		this.plazaRegistro = plazaRegistro;
	}

	public int getIdPlaza() {
		return idPlaza;
	}

	public void setIdPlaza(int idPlaza) {
		this.idPlaza = idPlaza;
	}

	public String getNombrePlaza() {
		return nombrePlaza;
	}

	public void setNombrePlaza(String nombrePlaza) {
		this.nombrePlaza = nombrePlaza;
	}

	public String getEstadoPlaza() {
		return estadoPlaza;
	}

	public void setEstadoPlaza(String estadoPlaza) {
		this.estadoPlaza = estadoPlaza;
	}

	public Set<Registro> getPlazaRegistro() {
		return plazaRegistro;
	}

	public void setPlazaRegistro(Set<Registro> plazaRegistro) {
		this.plazaRegistro = plazaRegistro;
	}

	@Override
	public String toString() {
		return "Plazas [idPlaza=" + idPlaza + ", nombrePlaza=" + nombrePlaza + ", estadoPlaza=" + estadoPlaza
				+ ", plazaRegistro=" + plazaRegistro + "]";
	}

}
