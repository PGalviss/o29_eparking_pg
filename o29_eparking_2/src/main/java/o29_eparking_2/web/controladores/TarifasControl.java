package o29_eparking_2.web.controladores;

import java.util.List;

import javax.servlet.annotation.WebServlet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import o29_eparking_2.web.entidades.Modalidad;
import o29_eparking_2.web.entidades.Tarifas;
import o29_eparking_2.web.entidades.TipoVehiculos;
import o29_eparking_2.web.repositorios.ModalidadRepository;
import o29_eparking_2.web.repositorios.TarifasRepositorio;
import o29_eparking_2.web.repositorios.TipoVehicRepository;
import o29_eparking_2.web.servicios.TarifasServicio;


@WebServlet("/tarifas")
@Controller
public class TarifasControl {
	
	@Autowired
	private TarifasServicio servicio;
	
	@Autowired
	private TarifasRepositorio tarifasRepositorio;
	
	//@Autowired
	//private TarifasRepositorio tarifasRepositorio;
	
	//@Autowired
	//private ModalidadServicio modalidadServicio;

	@Autowired
	private ModalidadRepository modalidadRepository;

	//@Autowired
	//private TipoVehicServicio tipoVehicServicio;
	
	@Autowired
	private TipoVehicRepository tipoVehicRepository;
	
	//Peticion para listar
	@GetMapping({"/tarifas/listaTarifas"}) //Escucha peticiones del Front End
	public String ListarTarifas(Model modelo) {
		modelo.addAttribute("tarifas", servicio.ListarTarifas());
		return "/tarifas/listaTarifas"; //un HTML llamado lista
	}
	
	//Peticion para mostrar formulario de crear tarifas
	@GetMapping("/tarifas/crearTarifas")
	public String formCrearTarifas(Model modelo) {
		List<Modalidad> listaModalidad = modalidadRepository.findAll();
		List<TipoVehiculos> listaTipoVehiculos = tipoVehicRepository.findAll();
		
		Tarifas objTarifas = new Tarifas();
		modelo.addAttribute("tarifas", objTarifas);
		modelo.addAttribute("listaModalidad", listaModalidad);
		modelo.addAttribute("listaTipoVehiculos", listaTipoVehiculos);
//		modelo.addAttribute("modalidad", modalidadServicio.listarModalidad());
//		modelo.addAttribute("tipoVehiculos", tipoVehicServicio.listarTipoVehic());
		return "/tarifas/crearTarifas"; //HTML de creacion
	}

	//Peticion para insertar una Tarifa 
	@PostMapping("/tarifas/listaTarifas")
	public String guardarTarifas(@ModelAttribute("tarifas") Tarifas objTarifas) {
		servicio.guardarTarifas(objTarifas);
		return "redirect:/tarifas/listaTarifas"; //retorna al HTML listar 
	}
	
	//PETICION PARA MOSTRAR FORM EDITAR 
	@GetMapping("/tarifas/editar/{id}")
	public String formularioEditar(@PathVariable int id, Model modelo) {
		//modelo.addAttribute("tarifas", servicio.tarifasXid(id)); 
		Tarifas tarifas = tarifasRepositorio.findById(id).get();
		modelo.addAttribute("tarifas", tarifas); 		
		
		List<Modalidad> listaModalidad = modalidadRepository.findAll();
		List<TipoVehiculos> listaTipoVehiculos = tipoVehicRepository.findAll();	
		
		modelo.addAttribute("listaModalidad", listaModalidad);
		modelo.addAttribute("listaTipoVehiculos", listaTipoVehiculos);	
		
		return "/tarifas/editarTarifas"; 
	}
	
	//PETICION PARA ACTUALIZAR EL CLIENTE
	@PostMapping("/tarifas/listaTarifas/{id}")
	public String editarTarifas(@PathVariable int id, 
							@ModelAttribute("tarifas") Tarifas objTarifas) {
		Tarifas tarifaExistente = servicio.tarifasXid(id); 
		tarifaExistente.setIdTarifa(id); 
		tarifaExistente.setEstadoTarifa(objTarifas.getEstadoTarifa());
		tarifaExistente.setValorTarifa(objTarifas.getValorTarifa());
		tarifaExistente.setModalidadTarifa(objTarifas.getModalidadTarifa());
		tarifaExistente.setTipoVehicTarifa(objTarifas.getTipoVehicTarifa());
		servicio.actualizarTarifas(tarifaExistente); 
		return "redirect:/cliente/listaTarifas"; 
	}
	
	@GetMapping("/tarifas/eliminarTarifas/{id}")
	public String borrarTarifa(@PathVariable int id) {
		servicio.eliminarTarifas(id);
		return "redirect:/tarifas/listaTarifas";
	}
	
}





