package o29_eparking_2.web.entidades;

import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name="vehiculos")
public class Vehiculos {
	//Atributos de la Clase
	@Id
	private String placa; 
	@Column(name="propietario", nullable=false, length=50)	
	private String propietario;  
	@Column(name="telefono", nullable=false, length=10)
	private String telefono; 
	@Column(name="marca", nullable=false, length=15)
	private String marca; 
	@Column(name="color", nullable=false, length=10)
	private String color; 
	//@Column(name="nombreModalidad", nullable=false)
	
	@OneToMany(mappedBy="placaRegistro")
	private Set<Registro> placaRegistro;
	
	@ManyToOne(cascade= {CascadeType.PERSIST,CascadeType.MERGE})
	@JoinColumn(name="modalidadVehiculo", referencedColumnName="idModalidad")
	private Modalidad modalidadVehiculo;	

	@ManyToOne(cascade= {CascadeType.PERSIST,CascadeType.MERGE})
	@JoinColumn(name="tipovehiculo", referencedColumnName="idTipoVehiculo")
	private TipoVehiculos tipovehiculo;	
	
	//Constructores
	public Vehiculos() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Vehiculos(String placa, String propietario, String telefono, String marca, String color,
			Set<Registro> placaRegistro, Modalidad modalidadVehiculo, TipoVehiculos tipovehiculo) {
		super();
		this.placa = placa;
		this.propietario = propietario;
		this.telefono = telefono;
		this.marca = marca;
		this.color = color;
		this.placaRegistro = placaRegistro;
		this.modalidadVehiculo = modalidadVehiculo;
		this.tipovehiculo = tipovehiculo;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getPropietario() {
		return propietario;
	}

	public void setPropietario(String propietario) {
		this.propietario = propietario;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Set<Registro> getPlacaRegistro() {
		return placaRegistro;
	}

	public void setPlacaRegistro(Set<Registro> placaRegistro) {
		this.placaRegistro = placaRegistro;
	}

	public Modalidad getModalidadVehiculo() {
		return modalidadVehiculo;
	}

	public void setModalidadVehiculo(Modalidad modalidadVehiculo) {
		this.modalidadVehiculo = modalidadVehiculo;
	}

	public TipoVehiculos getTipovehiculo() {
		return tipovehiculo;
	}

	public void setTipovehiculo(TipoVehiculos tipovehiculo) {
		this.tipovehiculo = tipovehiculo;
	}

	@Override
	public String toString() {
		return "Vehiculos [placa=" + placa + ", propietario=" + propietario + ", telefono=" + telefono + ", marca="
				+ marca + ", color=" + color + ", placaRegistro=" + placaRegistro + ", modalidadVehiculo="
				+ modalidadVehiculo + ", tipovehiculo=" + tipovehiculo + "]";
	}

}
