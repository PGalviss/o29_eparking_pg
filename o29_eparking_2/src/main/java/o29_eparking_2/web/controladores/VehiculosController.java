package o29_eparking_2.web.controladores;

import java.util.List;

import javax.servlet.annotation.WebServlet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import o29_eparking_2.web.entidades.Modalidad;
import o29_eparking_2.web.entidades.TipoVehiculos;
import o29_eparking_2.web.entidades.Vehiculos;
import o29_eparking_2.web.repositorios.ModalidadRepository;
import o29_eparking_2.web.repositorios.TipoVehicRepository;
import o29_eparking_2.web.repositorios.VehiculosRepository;

@WebServlet("/vehiculos")
@Controller
public class VehiculosController {
	
	@Autowired
	private VehiculosRepository vehiculosRepository;
	
	@Autowired
	private ModalidadRepository modalidadRepository;
	
	@Autowired
	private TipoVehicRepository tipoVehicRepository;

	//Listar los Vehiculos
	@GetMapping("/vehiculos/verVehiculos")
	public String ListarVehiculos(Model modelo) {
		List<Vehiculos> listaVehiculos = vehiculosRepository.findAll();
		modelo.addAttribute("listaVehiculos", listaVehiculos);
		return "/vehiculos/verVehiculos";	
	}

	//Peticion para mostrar formulario de crear vehiculos
	@GetMapping("/vehiculos/nuevo")
	public String formCrearVehiculos(Model modelo) {
		List<Modalidad> listaModalidad = modalidadRepository.findAll();
		List<TipoVehiculos> listaTipoVehiculos = tipoVehicRepository.findAll();
		
		Vehiculos objVehiculos = new Vehiculos();
		modelo.addAttribute("vehiculos", objVehiculos);
		modelo.addAttribute("listaModalidad", listaModalidad);
		modelo.addAttribute("listaTipoVehiculos", listaTipoVehiculos);
		return "/vehiculos/crearVehiculos"; //HTML de creacion
	}	
	
	//Peticion para insertar Vehiculos
	@PostMapping("/vehiculos/verVehiculos")
	public String guardarVehiculos(Vehiculos vehiculos) {
		vehiculosRepository.save(vehiculos);
		//servicio.guardarVehiculos(objVehiculos);
		return "redirect:/vehiculos/verVehiculos"; //retorna al HTML listar
	}
	
	//PETICION PARA MOSTRAR FORM EDITAR 
	@GetMapping("/vehiculos/editar/{id}")
	public String formularioEditar(@PathVariable String id, Model modelo) {
		Vehiculos vehiculos = vehiculosRepository.findById(id).get();
		modelo.addAttribute("vehiculos", vehiculos); 		
		
		List<Modalidad> listaModalidad = modalidadRepository.findAll();
		//List<TipoVehiculos> listaTipoVehiculos = tipoVehicRepository.findAll();	
		
		modelo.addAttribute("listaModalidad", listaModalidad);
		//modelo.addAttribute("listaTipoVehiculos", listaTipoVehiculos);	
		
		return "/vehiculos/editarVehiculos"; 
	}
	
	//Falta grabar edicion
	
	
	@GetMapping("/vehiculos/eliminar/{id}") 
	public String borrarVehiculosd(@PathVariable("id") String id) {
		vehiculosRepository.deleteById(id);
		return "redirect:/vehiculos/verVehiculos"; 
	}	
	
}
