package o29_eparking_2.web.entidades;

import java.sql.Date;
import java.sql.Time;
import javax.persistence.*;

@Entity
@Table(name="registro")
public class Registro {
	//Atributos de la clase
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private double ticket; 
	@Column(name="fechaIngreso", nullable=false)
	private Date fechaIngreso;
	@Column(name="horaIngreso", nullable=false)
	private Time horaIngreso; 
	@Column(name="fechaSalida", nullable=true)
	private Date fechaSalida; 
	@Column(name="horaSalida", nullable=true)
	private Time horaSalida;
	//@Column(name="tarifaRegistro", nullable=false)
	//private int tarifaRegistro;
	@Column(name="valorTarifa", nullable=false)
	private int valorTarifa; 
	@Column(name="estadoRegistro", nullable=false, length=10)
	private String estadoRegistro;
	//Foraneas
	@OneToOne(mappedBy="ticketFactura")
	private Factura ticketFactura;
	
	@ManyToOne(cascade= {CascadeType.PERSIST,CascadeType.MERGE})
	@JoinColumn(name="plazaRegistro", referencedColumnName="idPlaza")
	private Plazas plazaRegistro;
	
	@ManyToOne(cascade= {CascadeType.PERSIST,CascadeType.MERGE})
	@JoinColumn(name="placaRegistro", referencedColumnName="Placa")
	private Vehiculos placaRegistro;	
	
	@ManyToOne(cascade= {CascadeType.PERSIST,CascadeType.MERGE})
	@JoinColumn(name="tarifaRegistro", referencedColumnName="idTarifa")
	private Tarifas tarifaRegistro;

	public Registro() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Registro(double ticket, Date fechaIngreso, Time horaIngreso, Date fechaSalida, Time horaSalida,
			int valorTarifa, String estadoRegistro, Factura ticketFactura, Plazas plazaRegistro,
			Vehiculos placaRegistro, Tarifas tarifaRegistro) {
		super();
		this.ticket = ticket;
		this.fechaIngreso = fechaIngreso;
		this.horaIngreso = horaIngreso;
		this.fechaSalida = fechaSalida;
		this.horaSalida = horaSalida;
		this.valorTarifa = valorTarifa;
		this.estadoRegistro = estadoRegistro;
		this.ticketFactura = ticketFactura;
		this.plazaRegistro = plazaRegistro;
		this.placaRegistro = placaRegistro;
		this.tarifaRegistro = tarifaRegistro;
	}

	public double getTicket() {
		return ticket;
	}

	public void setTicket(double ticket) {
		this.ticket = ticket;
	}

	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public Time getHoraIngreso() {
		return horaIngreso;
	}

	public void setHoraIngreso(Time horaIngreso) {
		this.horaIngreso = horaIngreso;
	}

	public Date getFechaSalida() {
		return fechaSalida;
	}

	public void setFechaSalida(Date fechaSalida) {
		this.fechaSalida = fechaSalida;
	}

	public Time getHoraSalida() {
		return horaSalida;
	}

	public void setHoraSalida(Time horaSalida) {
		this.horaSalida = horaSalida;
	}

	public int getValorTarifa() {
		return valorTarifa;
	}

	public void setValorTarifa(int valorTarifa) {
		this.valorTarifa = valorTarifa;
	}

	public String getEstadoRegistro() {
		return estadoRegistro;
	}

	public void setEstadoRegistro(String estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	public Factura getTicketFactura() {
		return ticketFactura;
	}

	public void setTicketFactura(Factura ticketFactura) {
		this.ticketFactura = ticketFactura;
	}

	public Plazas getPlazaRegistro() {
		return plazaRegistro;
	}

	public void setPlazaRegistro(Plazas plazaRegistro) {
		this.plazaRegistro = plazaRegistro;
	}

	public Vehiculos getPlacaRegistro() {
		return placaRegistro;
	}

	public void setPlacaRegistro(Vehiculos placaRegistro) {
		this.placaRegistro = placaRegistro;
	}

	public Tarifas getTarifaRegistro() {
		return tarifaRegistro;
	}

	public void setTarifaRegistro(Tarifas tarifaRegistro) {
		this.tarifaRegistro = tarifaRegistro;
	}

	@Override
	public String toString() {
		return "Registro [ticket=" + ticket + ", fechaIngreso=" + fechaIngreso + ", horaIngreso=" + horaIngreso
				+ ", fechaSalida=" + fechaSalida + ", horaSalida=" + horaSalida + ", valorTarifa=" + valorTarifa
				+ ", estadoRegistro=" + estadoRegistro + ", ticketFactura=" + ticketFactura + ", plazaRegistro="
				+ plazaRegistro + ", placaRegistro=" + placaRegistro + ", tarifaRegistro=" + tarifaRegistro + "]";
	}	
	
	//Constructores
	

}
