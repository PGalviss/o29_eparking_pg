package o29_eparking_2.web.entidades;

import java.util.Set;

import javax.persistence.*;


@Entity
@Table(name="tarifas")
public class Tarifas {
	//Atributos de la clase
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idTarifa; 
	@Column(name="valorTarifa", nullable=false)
	private int valorTarifa; 
	@Column(name="estadoTarifa", nullable=false, length=10)
	private String estadoTarifa;
	
	@OneToMany(mappedBy="tarifaRegistro")
	private Set<Registro> tarifaRegistro;
	
	//@ManyToOne(cascade= {CascadeType.PERSIST,CascadeType.MERGE},fetch=FetchType.LAZY, optional=false)
	@ManyToOne()
	@JoinColumn(name="modalidadTarifa", referencedColumnName="idModalidad")
	private Modalidad modalidadTarifa;
	
	@ManyToOne(cascade= {CascadeType.PERSIST,CascadeType.MERGE},fetch=FetchType.LAZY, optional=false)
	//@ManyToOne()
	@JoinColumn(name="tipoVehicTarifa", referencedColumnName="idTipoVehiculo")
	private TipoVehiculos tipoVehicTarifa;
	
	//Constructores
	public Tarifas() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Tarifas(int idTarifa, int valorTarifa, String estadoTarifa, Set<Registro> tarifaRegistro,
			Modalidad modalidadTarifa, TipoVehiculos tipoVehicTarifa) {
		super();
		this.idTarifa = idTarifa;
		this.valorTarifa = valorTarifa;
		this.estadoTarifa = estadoTarifa;
		this.tarifaRegistro = tarifaRegistro;
		this.modalidadTarifa = modalidadTarifa;
		this.tipoVehicTarifa = tipoVehicTarifa;
	}

	public int getIdTarifa() {
		return idTarifa;
	}

	public void setIdTarifa(int idTarifa) {
		this.idTarifa = idTarifa;
	}

	public int getValorTarifa() {
		return valorTarifa;
	}

	public void setValorTarifa(int valorTarifa) {
		this.valorTarifa = valorTarifa;
	}

	public String getEstadoTarifa() {
		return estadoTarifa;
	}

	public void setEstadoTarifa(String estadoTarifa) {
		this.estadoTarifa = estadoTarifa;
	}

	public Set<Registro> getTarifaRegistro() {
		return tarifaRegistro;
	}

	public void setTarifaRegistro(Set<Registro> tarifaRegistro) {
		this.tarifaRegistro = tarifaRegistro;
	}

	public Modalidad getModalidadTarifa() {
		return modalidadTarifa;
	}

	public void setModalidadTarifa(Modalidad modalidadTarifa) {
		this.modalidadTarifa = modalidadTarifa;
	}

	public TipoVehiculos getTipoVehicTarifa() {
		return tipoVehicTarifa;
	}

	public void setTipoVehicTarifa(TipoVehiculos tipoVehicTarifa) {
		this.tipoVehicTarifa = tipoVehicTarifa;
	}

	@Override
	public String toString() {
		return "Tarifas [idTarifa=" + idTarifa + ", valorTarifa=" + valorTarifa + ", estadoTarifa=" + estadoTarifa
				+ ", tarifaRegistro=" + tarifaRegistro + ", modalidadTarifa=" + modalidadTarifa + ", tipoVehicTarifa="
				+ tipoVehicTarifa + "]";
	}

}
